<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryModel extends Model
{
    use HasFactory;
    protected $table = 'history';
    protected $guarded = [];

    public function getUser()
    {
        return $this->belongsTo('App\Models\User', 'id_author','id');
    }

    public function getDataDonor()
    {
        return $this->belongsTo('App\Models\IklanModel', 'id_donor','id');
    }

}
