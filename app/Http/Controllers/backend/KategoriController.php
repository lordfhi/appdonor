<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KategoriModel;
use Validator;
use Session;
class KategoriController extends Controller
{
    public function index()
    {
        $kategori = KategoriModel::all();
        return view('backend.page.kategori.index',compact('kategori'));
    }

    public function addForm()
    {
        return view('backend.page.kategori.addForm');
    }
    
    public function saveData(Request $request)
    {
        $input = $request->input('kategori');
        $kategori = KategoriModel::where('nama_kategori',$input)->first();
        if(!empty($kategori)){
            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Kategori Sudah Terdaftar !"
            ]);
            return redirect()->route('kategori.add');
        }


        $kategori = new KategoriModel;
        $kategori->nama_kategori = $request->input('nama_kategori');
        $kategori->save();

        return redirect()->route('kategori');
    }

    public function editForm($id)
    {
            $cek = KategoriModel::findOrFail($id);
            return view('backend.page.kategori.editForm',compact('cek'));
    }

    public function updateData(Request $request)
    {
        // dd($request->all());
        $id = $request->input('id');
        $cekdata = KategoriModel::where('nama_kategori',$request->input('nama_kategori'))->first();
        if(!empty($cekdata)){
            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Kategori Sudah Terupdate !"
            ]);
            return redirect()->back();
        }
        $kategori = KategoriModel::findOrFail($id);
        $kategori->nama_kategori = $request->input('nama_kategori');
        $kategori->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Kategori Berhasil Terupdate !"
        ]);
        return redirect()->route('kategori');
    }

    public function delete($id)
    {
        $kategori = KategoriModel::findOrFail($id);
        $kategori->delete();
        return redirect()->route('kategori');

    }
}
