<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutModel;

class AboutController extends Controller
{
    public function index()
    {
        $about = AboutModel::get();
        return view('backend.page.about.index',compact('about'));
    }
    
    public function edit($id)
    {
        $about = AboutModel::findOrFail($id);
        return view('backend.page.about.edit',compact('about'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $about = AboutModel::findOrFail($id);
        $about->deskripsi = $request->input('deskripsi');
        $about->save();
        return redirect()->route('about')->with('berhasil','Data Berhasil di Update');
    }
}
