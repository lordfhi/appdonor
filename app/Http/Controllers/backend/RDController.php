<?php

namespace App\Http\Controllers\backend;

use App\Models\HistoryModel;
use App\Models\IklanModel;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RDController extends Controller
{
    public function index()
    {
        $data = HistoryModel::where('id_author',Auth()->user()->id)->get();
        return view('backend.page.request.index',compact('data'));
    }

    public function ApproveDonor($id)
    {
        $data = HistoryModel::findOrFail($id);
        return view('backend.page.request.approve',compact('data'));
    }

    public function VerifikasiDonor(Request $request)
    {
        $id = $request->input('id');
        $data = HistoryModel::findOrFail($id);
        $data->catatan = $request->input('catatan');
        $data->status = $request->input('status');
        $data->save();

        return redirect()->route('requestDonor');
    }
}
