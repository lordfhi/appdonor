<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IklanModel;
use App\Models\KategoriModel;
class IklanController extends Controller
{
    public function index()
    {
        $data = IklanModel::where('id_author',Auth()->user()->id)->get();
        return view('backend.page.iklan.index',compact('data'));
    }

    public function addForm()
    {
       $kategori = KategoriModel::get();
       return view('backend.page.iklan.addForm',compact('kategori'));
    }

    public function saveData(Request $request)
    {
        // dd($request->all());
        $data = new IklanModel;
        $data->judul = $request->input('judul');
        $data->deskripsi = $request->input('deskripsi');
        $data->no_hp = $request->input('no_hp');
        $data->lokasi = $request->input('lokasi');
        $data->level = $request->input('level');
        $data->id_kategori = $request->input('id_kategori');
        $data->id_author = Auth()->user()->id;
        $data->save();
        return redirect()->route('iklan');
    }

    public function editForm($id)
    {
       $kategori = KategoriModel::get();
       $edit = IklanModel::findOrFail($id);
       return view('backend.page.iklan.editForm',compact('edit','kategori'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $data = IklanModel::findOrFail($id);
        $data->judul = $request->input('judul');
        $data->deskripsi = $request->input('deskripsi');
        $data->no_hp = $request->input('no_hp');
        $data->lokasi = $request->input('lokasi');
        $data->level = $request->input('level');
        $data->id_kategori = $request->input('id_kategori');
        $data->save();
        return redirect()->route('iklan');
    }
    public function delete($id)
    {
       $iklan = IklanModel::findOrFail($id);
       $iklan->delete();
       return redirect()->route('iklan');
    }
}
