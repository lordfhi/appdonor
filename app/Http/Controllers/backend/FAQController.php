<?php

namespace App\Http\Controllers\backend;
use App\Models\FAQModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FAQController extends Controller
{
    public function index()
    {
        $faq = FAQModel::all();
        return view('backend.page.faq.index',compact('faq'));
    }
    
    public function create()
    {
        return view('backend.page.faq.form');
    }

    public function save(Request $request)
    {
        $data = new FAQModel;
        $data->judul = $request->input('judul');
        $data->deskripsi = $request->input('deskripsi');
        $data->save();

        return redirect()->route('faqs')->with('berhasil','Berhasil menambah FAQ !');
    }

    public function edit($id)
    {
        $faq = FAQModel::findOrFail($id);
        return view('backend.page.faq.edit',compact('faq'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $data = FAQModel::findOrFail($id);
        $data->judul = $request->input('judul');
        $data->deskripsi = $request->input('deskripsi');
        $data->save();

        return redirect()->route('faqs')->with('berhasil','Berhasil merubah data FAQ !');
    }
    
    public function delete($id)
    {
        $data = FAQModel::findOrFail($id);
        $data->delete();
        
        return redirect()->route('faqs')->with('berhasil','Berhasil menghapus data FAQ !');
    }
}
