<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\IklanModel;
use App\Models\HistoryModel;
use Illuminate\Http\Request;

class SiteController extends Controller
{
  
    public function dashboard()
    {
        $iklan = IklanModel::where('id_author',Auth()->user()->id)->get();
        $countIklan = count($iklan);

        $donor = HistoryModel::where('id_author',Auth()->user()->id)->get();
        $countDonor = count($donor); 
        return view('backend.page.dashboard.index',compact('countIklan','countDonor'));
    }

    public function about()
    {
        
    }
}
