<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\RoleModel;

class LoginController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }
    
    
    public function auth(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        // dd($request->all());
        
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            
            // if(Auth::user()->role == 1 || Auth::user()->role == 3){
                return redirect()->route('dashboard');
            // }else{
            //     return redirect()->route('home');
            // }
        }
        
        return redirect()->route('login')->with('alert','Email atau Password anda salah!');
    }
    
    public function register()
    {
        $role = RoleModel::get()->except(['nama_role'=>'Admin']);
        return view('auth.register',compact('role'));
    }
    
    public function saveRegister(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'nama'     => 'required',
            'email'     => 'required',
            'no_hp'     => 'required',
            'role'     => 'required',
            'alamat'     => 'required',
            'password'     => 'required',
        ]);
        
        if ($validator->fails()) {
            \Session::flash('errors', $validator->errors());
            return redirect()->back()->with('message', 'Harap isi pengecekan data dengan valid.');
        }
        
        // dd($request->all());
        
        $register = new User;
        $cekUser = User::where('email',$request->email)->first();
        if(!empty($cekUser)){
            return redirect()->back()->with('message', 'Harap isi pengecekan data dengan valid.');
        }else{
            $register->nama = $request->input('nama');
            $register->email = $request->input('email');
            $register->no_hp = $request->input('no_hp');
            $register->role = $request->input('role');
            $register->alamat = $request->input('alamat');
            $register->password = bcrypt($request->input('password'));
            $register->save();
            return redirect()->route('login')->with('berhasil','Terimakasih Telah mendaftar ke aplikasi kami ');
        }
        
        // return redirect()->route('register');
        
    }
    
    public function logout(Request $request)
    {
        Auth::logout();
        
        $request->session()->invalidate();
        
        $request->session()->regenerateToken();
        
        return redirect('/login');
    }
    
}
