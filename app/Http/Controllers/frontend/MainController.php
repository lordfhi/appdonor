<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IklanModel;
use App\Models\HistoryModel;
use App\Models\AboutModel;
use App\Models\FAQModel;


class MainController extends Controller
{
    public function index()
    {
        $iklan = IklanModel::limit(4)->get();
        $about = AboutModel::limit(1)->get();
        return view('frontend.layouts.master',compact('iklan','about'));
    }
    
    public function donordarah($id)
    {
        
        $iklan = IklanModel::limit(4)->get();
        $donor = IklanModel::where('id_kategori',$id)->simplePaginate(9);
        $about = AboutModel::limit(1)->get();
        return view('frontend.page.donor.content',compact('donor','iklan','about'));
    }

    public function donor()
    {
        
        $iklan = IklanModel::limit(4)->get();
        $donor = IklanModel::simplePaginate(9);
        $about = AboutModel::limit(1)->get();
        return view('frontend.page.donor.content',compact('donor','iklan','about'));
    }
    
    public function detail($id)
    {
        
        $iklan = IklanModel::limit(4)->get();
        $donor = IklanModel::findOrFail($id);
        $about = AboutModel::limit(1)->get();
        return view('frontend.page.donor.detaildata',compact('donor','iklan','about'));
    }

    public function about()
    {
        
        $iklan = IklanModel::limit(4)->get();
        $about = AboutModel::limit(1)->get();
        return view('frontend.page.about.index',compact('iklan','about'));
    }

    public function faq()
    {
        
        $about = AboutModel::limit(1)->get();
        $faq = FAQModel::get();
        $iklan = IklanModel::limit(4)->get();
        return view('frontend.page.faq.index',compact('iklan','about','faq'));
    }

    
}
