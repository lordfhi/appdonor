<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HistoryModel;
use App\Models\AboutModel;
use App\Models\IklanModel;

class HistoryController extends Controller
{
    public function index($id)
    {
        $data = HistoryModel::where('id_users',$id)->get();
        
        $about = AboutModel::limit(1)->get();
        $iklan = IklanModel::limit(4)->get();
        return view('frontend.page.history.index',compact('data','iklan','about'));
    }

    public function detailData($id)
    {
     
        $iklan = IklanModel::limit(4)->get();
        $data = HistoryModel::where('id_users',$id)->first();
        
        $about = AboutModel::limit(1)->get();
        return view('frontend.page.history.dataDetail',compact('data','iklan','about'));   
    }
}
