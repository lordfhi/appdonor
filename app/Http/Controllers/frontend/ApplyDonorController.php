<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HistoryModel;
use App\Models\IklanModel;

class ApplyDonorController extends Controller
{

    public function applyData(Request $request)
    {
        $id = $request->input('id_donor');
        $dataDonor = IklanModel::where('id',$id)->first();
        // dd($dataDonor->id_author);
        $data = new HistoryModel;
        $data->id_users = Auth()->user()->id;
        $data->id_author = $dataDonor->id_author;
        $data->id_donor = $request->input('id_donor');
        $data->save();
        
        return redirect()->route('home')->with('berhasil','Terima Kasih sudah ikut berpartisipasi sahabatku !');
    }
}
