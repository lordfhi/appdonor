<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile dropdown m-t-20">
                        <div class="user-pic">
                            <img src="{{asset('backend')}}/assets/user.png" alt="users" class="rounded-circle img-fluid" />
                        </div>
                        <div class="user-content hide-menu m-t-10">
                            <h5 class="m-b-10 user-name font-medium">{{Auth()->user()->nama}}</h5>
                            <div class="dropdown-menu animated flipInY" aria-labelledby="Userdd">
                                <a class="dropdown-item" href="javascript:void(0)">
                                    
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                        <a class="dropdown-item" href="javascript:void(0)">
                                            <i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="javascript:void(0)">
                                                <i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="javascript:void(0)">
                                                    <i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End User Profile-->
                                    </li>
                                    <!-- User Profile-->
                                    
                                    <li class="nav-small-cap">
                                        <i class="mdi mdi-dots-horizontal"></i>
                                        <span class="hide-menu">Dashboard</span>
                                    </li>
                                    <li class="sidebar-item">
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('dashboard')}}" aria-expanded="false">
                                            <i class="mdi mdi-content-paste"></i>
                                            <span class="hide-menu">Dashboard</span>
                                        </a>
                                    </li>
                                   
                                    @if (Auth()->user()->role == 3)
                                    <li class="nav-small-cap">
                                        <i class="mdi mdi-dots-horizontal"></i>
                                        <span class="hide-menu">Master Data</span>
                                    </li>
                                    <li class="sidebar-item">
                                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                            <i class="icon-Mailbox-Empty"></i>
                                            <span class="hide-menu">Kategori </span>
                                        </a>
                                        <ul aria-expanded="false" class="collapse  first-level">
                                            <li class="sidebar-item">
                                                <a href="{{route('kategori')}}" class="sidebar-link">
                                                    <i class="mdi mdi-email"></i>
                                                    <span class="hide-menu"> Kategori </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="sidebar-item">
                                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                            <i class="icon-Mailbox-Empty"></i>
                                            <span class="hide-menu">Website </span>
                                        </a>
                                        <ul aria-expanded="false" class="collapse  first-level">
                                            <li class="sidebar-item">
                                                <a href="{{route('about')}}" class="sidebar-link">
                                                    <i class="mdi mdi-email"></i>
                                                    <span class="hide-menu"> About </span>
                                                </a>
                                            </li>
                                            <li class="sidebar-item">
                                                <a href="{{route('faqs')}}" class="sidebar-link">
                                                    <i class="mdi mdi-email"></i>
                                                    <span class="hide-menu"> FAQ </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    @else
                                    
                                    <li class="nav-small-cap">
                                        <i class="mdi mdi-dots-horizontal"></i>
                                        <span class="hide-menu">DATA TRANSAKSI</span>
                                    </li>
                                    <li class="sidebar-item">
                                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                            <i class="icon-Paint-Brush"></i>
                                            <span class="hide-menu">Iklan </span>
                                        </a>
                                        <ul aria-expanded="false" class="collapse first-level">
                                            <li class="sidebar-item">
                                                <a href="{{route('iklan')}}" class="sidebar-link">
                                                    <i class="mdi mdi-toggle-switch"></i>
                                                    <span class="hide-menu">Buat Iklan</span>
                                                </a>
                                            </li>
                                            <li class="sidebar-item">
                                                <a href="{{route('requestDonor')}}" class="sidebar-link">
                                                    <i class="mdi mdi-tablet"></i>
                                                    <span class="hide-menu">Request Donor</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    
                                    <li class="sidebar-item">
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('home')}}" aria-expanded="false">
                                            <i class="mdi mdi-home"></i>
                                            <span class="hide-menu">Halaman Pendonor</span>
                                        </a>
                                    </li>
                                    @endif
                                    <li class="nav-small-cap">
                                        <i class="mdi mdi-dots-horizontal"></i>
                                        <span class="hide-menu">Keluar</span>
                                    </li>
                                    {{-- <li class="sidebar-item">
                                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{asset('backend')}}/docs/documentation.html
                                        " aria-expanded="false">
                                        <i class="ti-settings"></i>
                                        <span class="hide-menu">Setting</span>
                                          </a>
                                    </li> --}}
                                <li class="sidebar-item">
                                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('logout')}}" aria-expanded="false">
                                        <i class="mdi mdi-directions"></i>
                                        <span class="hide-menu">Log Out</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <!-- End Sidebar navigation -->
                    </div>
                    <!-- End Sidebar scroll-->
                </aside>