@extends('backend.layouts.master')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Tambah Iklan</h4>
                <form class="m-t-30" method="POST" action="{{route('iklan.save')}}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Judul Iklan</label>
                        <input type="text" class="form-control" name="judul" placeholder="Judul Iklan" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kategori</label>
                        <select class="form-control" name="id_kategori" required>
                            <option value="">-- PILIH KATEGORI --</option>
                                @foreach ($kategori as $row)
                                    <option value="{{$row->id}}">{{$row->nama_kategori}}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi Iklan</label>
                       <textarea name="deskripsi"  class="form-control" id="" cols="30" rows="10" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nomor HandPhone</label>
                        <input type="text" class="form-control" name="no_hp" placeholder="Nomor HandPhone" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Lokasi</label>
                        <input type="text" class="form-control" name="lokasi" placeholder="Lokasi" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Level</label>
                        <select name="level" class="form-control" required>
                            <option value="">-- PILIH LEVEL -- </option>
                            <option>Urgent</option>
                            <option>Tidak Urgent</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" style="float: right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection