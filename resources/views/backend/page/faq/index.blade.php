@extends('backend.layouts.master')

@section('css')
    
<link href="{{asset('backend')}}/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="card-title">Data FAQ</h4>
                    </div>
                    <div class="col-md-2" style="padding-left: 9%">
                        <a href="{{route('faqs.create')}}" class="btn btn-primary">Tambah FAQ</a>
                    </div>
                </div>
                @if(\Session::has('alert'))
                <div class="alert alert-danger">
                    <div>{{Session::get('alert')}}</div>
                </div>
                @elseif(\Session::get('berhasil'))
                <div class="alert alert-success">
                    <div>{{Session::get('berhasil')}}</div>
                </div>
                @endif
                <br>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                           @foreach ($faq as $row )
                            <tr>
                                <td width="1%"><?php echo $no++; ?></td>
                                <td>{{$row->judul}}</td>
                                <td>{{$row->deskripsi}}</td>
                                <td><a href="{{route('faqs.edit',$row->id)}}" class="btn btn-warning">Edit</a>&nbsp;<a href="{{route('faqs.delete',$row->id)}}" class="btn btn-danger">Hapus</a></td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    
    <!--This page plugins -->
    <script src="{{asset('backend')}}/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="{{asset('backend')}}/dist/js/pages/datatable/datatable-basic.init.js"></script>
@endsection