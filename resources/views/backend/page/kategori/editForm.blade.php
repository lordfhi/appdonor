@extends('backend.layouts.master')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Edit Kategori</h4>
                @if(session()->has('flash_notification.message'))
                <div class="alert alert-{{ session()->get('flash_notification.level') }}">
                    {!! session()->get('flash_notification.message') !!}
                </div>
                @endif
                <form class="m-t-30" method="POST" action="{{route('kategori.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$cek->id}}">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kategori</label>
                        <input type="text" class="form-control" name="nama_kategori" value="{{$cek->nama_kategori}}" placeholder="Nama Kategori">
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" style="float: right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection