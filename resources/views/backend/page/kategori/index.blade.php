@extends('backend.layouts.master')

@section('css')
    
<link href="{{asset('backend')}}/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">

                        <h4 class="card-title">Data Master Kategori</h4>
                        <h6 class="card-subtitle">Kumpulan Data Kategori</h6>
                    </div>
                    <div class="col-md-2" style="padding-left: 7%">
                        <a href="{{route('kategori.add')}}" class="btn btn-primary">Tambah Kategori</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Nama Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                           @foreach ($kategori as $row )
                            <tr>
                                <td width="1%"><?php echo $no++; ?></td>
                                <td>{{$row->nama_kategori}}</td>
                                <td><a href="{{route('kategori.edit',$row->id)}}" class="btn btn-warning">Edit</a> &nbsp; <a href="{{route('kategori.delete',$row->id)}}" class="btn btn-danger">Hapus</a></td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    
    <!--This page plugins -->
    <script src="{{asset('backend')}}/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="{{asset('backend')}}/dist/js/pages/datatable/datatable-basic.init.js"></script>
@endsection