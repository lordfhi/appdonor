@extends('backend.layouts.master')

@section('css')
    
<link href="{{asset('backend')}}/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="card-title">Data About</h4>
                    </div>
                </div>
                @if(\Session::has('alert'))
                <div class="alert alert-danger">
                    <div>{{Session::get('alert')}}</div>
                </div>
                @elseif(\Session::get('berhasil'))
                <div class="alert alert-success">
                    <div>{{Session::get('berhasil')}}</div>
                </div>
                @endif
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Judul</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                           @foreach ($about as $row )
                            <tr>
                                <td width="1%"><?php echo $no++; ?></td>
                                <td>{{$row->deskripsi}}</td>
                                <td><a href="{{route('about.edit',$row->id)}}" class="btn btn-warning">Edit</a></td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    
    <!--This page plugins -->
    <script src="{{asset('backend')}}/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="{{asset('backend')}}/dist/js/pages/datatable/datatable-basic.init.js"></script>
@endsection