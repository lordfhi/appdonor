@extends('backend.layouts.master')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Edit About</h4>
                @if(session()->has('flash_notification.message'))
                <div class="alert alert-{{ session()->get('flash_notification.level') }}">
                    {!! session()->get('flash_notification.message') !!}
                </div>
                @endif
                <form class="m-t-30" method="POST" action="{{route('about.update')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$about->id}}">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi About</label>
                        {{-- <input type="text" class="form-control" name="deskripsi" value="{{$about->deskripsi}}" placeholder="Nama Kategori"> --}}
                        <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$about->deskripsi}}</textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" style="float: right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection