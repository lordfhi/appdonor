@extends('backend.layouts.master')



@section('content')
<div class="card-group">
    <!-- Card -->
    <div class="card">
        <div class="card-body">
            <div class="d-flex align-items-center">
                <div class="m-r-10">
                    <span class="btn btn-circle btn-lg bg-danger">
                        <i class="ti-clipboard text-white"></i>
                    </span>
                </div>
                <div>
                    Total Iklan
                </div>
                <div class="ml-auto">
                    <h2 class="m-b-0 font-light">{{$countIklan}}</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- Card -->
    <!-- Card -->
    <div class="card">
        <div class="card-body">
            <div class="d-flex align-items-center">
                <div class="m-r-10">
                    <span class="btn btn-circle btn-lg btn-info">
                        <i class="ti-wallet text-white"></i>
                    </span>
                </div>
                <div>
                    Total Pendonor

                </div>
                <div class="ml-auto">
                    <h2 class="m-b-0 font-light">{{$countDonor}}</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- Card -->
    <!-- Column -->


</div>
@endsection