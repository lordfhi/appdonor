@extends('backend.layouts.master')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Pengaju</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Pengaju</label>
                                <input type="text" class="form-control" name="nama_kategori" value="{{$data->getUser->nama}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nomor HandPhone</label>
                                <input type="text" class="form-control" name="nama_kategori" value="{{$data->getUser->no_hp}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Donor</label>
                                <input type="text" class="form-control" name="nama_kategori" value="{{$data->getDataDonor->judul}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat Pendonor</label>
                                <input type="text" class="form-control" name="nama_kategori" value="{{$data->getUser->alamat}}" disabled>
                            </div>
                        </div>
                    </div>
                    
                <form class="m-t-30" method="POST" action="{{route('requestDonor.verifikasi')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <div class="row">
                       @if ($data->status == 0)
                       <div class="col-md-12">
                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea name="catatan" rows="3" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" value="1" name="status" class="btn btn-danger  ml-3" style="float: right">Tolak</button>
                        <button type="submit" value="2" name="status" class="btn btn-primary" style="float: right">Simpan</button>
                    </div>
                    @else
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea name="catatan" rows="3" class="form-control">{{$data->catatan}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a href="{{route('requestDonor')}}" class="btn btn-primary" style="float: right">Kembali</a>
                    </div>
                       @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection