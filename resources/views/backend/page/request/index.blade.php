@extends('backend.layouts.master')

@section('css')
    
<link href="{{asset('backend')}}/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-10">

                        <h4 class="card-title">Data Pengaju Pendonor</h4>
                        <h6 class="card-subtitle">Kumpulan Data Pengaju Pendonor</h6>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th>Nama Pengaju</th>
                                <th>Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                           @foreach ($data as $row )
                            <tr>
                                <td width="1%"><?php echo $no++; ?></td>
                                <td>{{$row->getUser->nama}}</td>
                                <td>{{$row->getDataDonor->judul}}</td>
                                @if ($row->status == 0)
                                <td><a href="{{route('requestDonor.approve',$row->id)}}" class="btn btn-primary">Approve</a></td>
                                @else
                                <td><a href="{{route('requestDonor.approve',$row->id)}}" class="btn btn-success">Detail</a></td>
                                @endif
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    
    <!--This page plugins -->
    <script src="{{asset('backend')}}/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="{{asset('backend')}}/dist/js/pages/datatable/datatable-basic.init.js"></script>
@endsection