	<!-- BEGIN #header -->
    <div id="header" class="header">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN header-container -->
            <div class="header-container">
                <!-- BEGIN navbar-toggle -->
                <button type="button" class="navbar-toggle collapsed" data-bs-toggle="collapse" data-bs-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- END navbar-toggle -->
                <!-- BEGIN header-logo -->
                <div class="header-logo">
                    <a href="{{route('home')}}">
                        <span class="brand-logo"></span>
                        <span class="brand-text">
                            <span>Aplikasi</span>Pendonor
                            <small></small>
                        </span>
                    </a>
                </div>
                <!-- END header-logo -->
                <!-- BEGIN header-nav -->
                <div class="header-nav">
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav justify-content-center">
                            <li class="active"><a href="{{route('home')}}">Home</a></li>
                            <li><a href="{{route('abouts')}}">About</a></li>
                            <li><a href="{{route('faq')}}">FAQ</a></li>
                            @if (!empty(Auth()->user()->id))
                            <li>  
                                <a href="{{route('dashboard')}}">
                                    <span class="d-none d-xl-inline">Dashboard</span>
                                </a>
                            </li>
                            <li>  
                                <a href="{{route('history',Auth()->user()->id)}}">
                                    <span class="d-none d-xl-inline">History</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                        </div>
                    </div>
                    <!-- END header-nav -->
                    <!-- BEGIN header-nav -->
                    <div class="header-nav">
                        <ul class="nav justify-content-end">
                            <li>
                                @if (!empty(Auth()->user()->id))
                                <a href="{{route('logout')}}">
                                    <img src="{{asset('frontend/')}}/assets/user.png" class="user-img" alt="" /> 
                                    <span class="d-none d-xl-inline">Logout</span>
                                </a>
                                @else
                                <a href="{{route('login')}}">
                                    <img src="{{asset('frontend/')}}/assets/user.png" class="user-img" alt="" /> 
                                    <span class="d-none d-xl-inline">Login / Register</span>
                                </a>
                                @endif
                            </li>
                        </ul>
                    </div>
                    <!-- END header-nav -->
                </div>
                <!-- END header-container -->
            </div>
            <!-- END container -->
        </div>
        <!-- END #header -->