	<!-- BEGIN #footer -->
    <div id="footer" class="footer">
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN row -->
            <div class="row">
                <!-- BEGIN col-3 -->
                <div class="col-lg-3">
                    <h4 class="footer-header">ABOUT US</h4>
                    @foreach ($about as $row)
                    <p>
                        {{$row->deskripsi}}
                    </p>
                    @endforeach
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-lg-3">
                    <h4 class="footer-header">RELATED LINKS</h4>
                    <ul class="fa-ul mb-lg-4 mb-0 p-0">
                        <li><i class="fa fa-fw fa-angle-right"></i> <a href="{{route('home')}}">Home</a></li>
                        <li><i class="fa fa-fw fa-angle-right"></i> <a href="{{route('about')}}">About</a></li>
                        <li><i class="fa fa-fw fa-angle-right"></i> <a href="{{route('faq')}}">FAQ</a></li>
                    </ul>
                </div>
                <!-- END col-3 -->
                <!-- BEGIN col-3 -->
                <div class="col-lg-3">
                    <h4 class="footer-header">LATEST DONOR</h4>
                    <ul class="list-unstyled list-product mb-lg-4 mb-0 p-0">
                        @foreach ($iklan as $row)
                        <li>
                            <i class="fa fa-fw fa-angle-right"></i>
                            <div class="info">
                               <a href="{{route('donordarah.detail',$row->id)}}"> <h4 class="info-title">{{$row->judul}}</h4></a>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- END col-3 -->
            </div>
            <!-- END row -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #footer -->