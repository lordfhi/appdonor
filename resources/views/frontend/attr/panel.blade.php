	<!-- BEGIN #about-us-cover -->
    <div id="about-us-cover" class="has-bg section-container">
        <!-- BEGIN cover-bg -->
        <div class="cover-bg">
            <img src="{{asset('frontend/')}}/assets/img/cover/cover-13.jpg" alt="" />
            {{-- <img src="{{asset('')}}logodonor.jpg" alt="" /> --}}
        </div>
        <!-- END cover-bg -->
        <!-- BEGIN container -->
        <div class="container">
            <!-- BEGIN breadcrumb -->
            <!-- END breadcrumb -->
            <!-- BEGIN about-us -->
            <div class="about-us text-center">
                <h1>Perjalanan kebaikanmu dimulai di sini</h1>
                <p>
                    Aksiku hari ini akan menyelamatkan nyawa seseorang esok hari
                </p>
            </div>
            <!-- END about-us -->
        </div>
        <!-- END container -->
    </div>
    <!-- END #about-us-cover -->