<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Aplikasi Pendonor - 2021</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    <!-- ================== BEGIN core-css ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{asset('frontend/')}}/assets/css/e-commerce/vendor.min.css" rel="stylesheet" />
    <link href="{{asset('frontend/')}}/assets/css/e-commerce/app.min.css" rel="stylesheet" />
    <!-- ================== END core-css ================== -->
</head>
<body>
    <!-- BEGIN #page-container -->
    <div id="page-container" class="fade show">
        @include('frontend.attr.navbar')
        @include('frontend.attr.panel')
        
        <!-- BEGIN #about-us-content -->
        <div id="about-us-content" class="section-container bg-white">
            <!-- BEGIN container -->
            <div class="container">
                <!-- BEGIN about-us-content -->
                <div class="about-us-content">
                    <h2 class="title text-center">Apa yang kami bisa bantu?</h2>
                    <p class="desc text-center">
                        Aplikasi Donor ini kami buat untuk kalian yang ingin mendonor. <br> Kami sangat berterima kasih sekali kepada teman-teman yang ingin mendonor. 
                    </p>
                    @if(\Session::has('alert'))
                    <div class="alert alert-danger">
                        <div>{{Session::get('alert')}}</div>
                    </div>
                    @elseif(\Session::get('berhasil'))
                    <div class="card shadow border-0 mb-5">
                        <div class="card-body p-4">
                            <div class="mb-3 w-50px h-50px rounded-3 bg-teal text-white d-flex align-items-center justify-content-center position-relative">
                                <i class="fa fa-check fs-28px"></i>
                            </div>
                            <h4>{{Session::get('berhasil')}}</h4>
                        </div>
                    </div>
                    @endif
                    <!-- BEGIN row -->
                    <div class="row">
                        <!-- begin col-4 -->
                        <div class="col-md-3 col-sm-3">
                            <div class="service">
                                <a href="{{route('donordarah',6)}}">
                                    <div class="icon text-info"><i class="fa fa-medkit"></i></div>
                                    <div class="info">
                                        <h4 class="title">Donor Darah</h4>
                                        <p class="desc"></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- end col-4 -->
                        <!-- begin col-4 -->
                        <div class="col-md-3 col-sm-3">
                            <div class="service">
                                <a href="{{route('donordarah',7)}}">
                                    <div class="icon text-info"><i class="fa fa-heartbeat"></i></div>
                                    <div class="info">
                                        <h4 class="title">Donor Plasma Covid-19</h4>
                                        <p class="desc"></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- end col-4 -->
                        <!-- begin col-4 -->
                        <div class="col-md-3 col-sm-3">
                            <div class="service">
                                <a href="{{route('donordarah',4)}}">
                                    <div class="icon text-info"><i class="fa fa-plus-square"></i></div>
                                    <div class="info">
                                        <h4 class="title">Donor Jantung</h4>
                                        <p class="desc"></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- end col-4 -->
                          <!-- begin col-4 -->
                          <div class="col-md-3 col-sm-3">
                            <div class="service">
                                <a href="{{route('donor')}}">
                                    <div class="icon text-info"><i class="fa fa-user-md"></i></div>
                                    <div class="info">
                                        <h4 class="title">Semua Donor</h4>
                                        <p class="desc"></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- end col-4 -->
                    </div>
                    <!-- END row -->
                </div>
                <!-- END about-us-content -->
            </div>
            <!-- END container -->
        </div>
        <!-- END #about-us-content -->
        
        @include('frontend.attr.footer')
        
        <!-- BEGIN #footer-copyright -->
        <div id="footer-copyright" class="footer-copyright">
            <!-- BEGIN container -->
            <div class="container">
                <div class="copyright">
                    {{-- Copyright &copy; 2021 DhanesaTech. All rights reserved. --}}
                </div>
            </div>
            <!-- END container -->
        </div>
        <!-- END #footer-copyright -->
    </div>
    <!-- END #page-container -->
    
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('frontend/')}}/assets/js/e-commerce/vendor.min.js"></script>
    <script src="{{asset('frontend/')}}/assets/js/e-commerce/app.min.js"></script>
    
</body>
</html>