<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Aplikasi Pendonor - 2021</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    <!-- ================== BEGIN core-css ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{asset('frontend/')}}/assets/css/e-commerce/vendor.min.css" rel="stylesheet" />
    <link href="{{asset('frontend/')}}/assets/css/e-commerce/app.min.css" rel="stylesheet" />
    <!-- ================== END core-css ================== -->
</head>
<body>
    <!-- BEGIN #page-container -->
    <div id="page-container" class="fade show">
        @include('frontend.attr.navbar')
        @include('frontend.attr.panel')
        
        <!-- BEGIN #about-us-content -->
        <div id="about-us-content" class="section-container bg-white">
            <!-- BEGIN container -->
            <div class="container">
                <!-- BEGIN about-us-content -->
              @yield('content')
                <!-- END about-us-content -->
            </div>
            <!-- END container -->
        </div>
        <!-- END #about-us-content -->
        
        @include('frontend.attr.footer')
        
        <!-- BEGIN #footer-copyright -->
        <div id="footer-copyright" class="footer-copyright">
            <!-- BEGIN container -->
            <div class="container">
                <div class="copyright">
                    {{-- Copyright &copy; 2021 DhanesaTech. All rights reserved. --}}
                </div>
            </div>
            <!-- END container -->
        </div>
        <!-- END #footer-copyright -->
    </div>
    <!-- END #page-container -->
    
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('frontend/')}}/assets/js/e-commerce/vendor.min.js"></script>
    <script src="{{asset('frontend/')}}/assets/js/e-commerce/app.min.js"></script>
    
</body>
</html>