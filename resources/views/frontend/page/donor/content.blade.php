@extends('frontend.layouts.masterDetail')



@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="{{route('home')}}" class="btn btn-primary" style="float: left"> <i class="fa fa-arrow-left"></i> Back</a>
    </div>
    <!-- BEGIN col-4 -->
    <br>
    <br>
    <br>
    @foreach ($donor as $row)
    <div class="col-lg-4">
        <!-- BEGIN card -->
        <div class="card shadow border-0 mb-5">
            <div class="card-body p-4">
                <div class="mb-3 w-50px h-50px rounded-3 bg-red text-white d-flex align-items-center justify-content-center position-relative">
                    <i class="fa fa-medkit fs-28px"></i>
                </div>
                <h4>{{$row->judul}}</h4>
                <p class="fw-bold text-gray-600 mb-0">
                  {{$row->deskripsi}}
                </p>
                <a href="{{route('donordarah.detail',$row->id)}}" class="stretched-link"></a>
            </div>
        </div>
        <!-- END card -->
    </div>        
    @endforeach
    {{ $donor->links() }}

    <!-- END col-4 -->
</div>

@endsection