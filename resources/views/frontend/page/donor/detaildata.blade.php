@extends('frontend.layouts.masterDetail')

@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="{{route('home')}}" class="btn btn-primary" style="float: left"> <i class="fa fa-arrow-left"></i> Back</a>
        <br><br>
    </div>
</div>
<div class="modal-content">
    <div class="modal-header px-4">
        <div class="fs-24px fw-bolder">Dibutuhkan Segera {{$donor->judul}}</div>
    </div>
    <div class="modal-body p-0">
        
        <div class="row gx-0">
            <div class="col-md-8 p-4 border-end fs-14px line-h-16">
                <p class="mb-0">
                    {{$donor->deskripsi}}    
                </p>
            </div>
            <div class="col-md-4 p-4">
                <hr class="d-block d-md-none mt-n5">
                <div class="d-flex align-items-center mb-3">
                    <div class="me-3 w-40px h-40px rounded-2 bg-gray-400 text-gray-600 d-md-flex d-none align-items-center justify-content-center">
                        <i class="far fa-fw fa-map fa-lg"></i>
                    </div>
                    <div>
                        <div class="fw-bold text-gray-600 fs-12px line-h-12">Location:</div>
                        <div class="fw-bold text-gray-800">{{$donor->alamat}}</div>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div class="me-3 w-40px h-40px rounded-2 bg-gray-400 text-gray-600 d-md-flex d-none align-items-center justify-content-center">
                        <i class="fa fa-fw fa-medkit fa-lg"></i>
                    </div>
                    <div>
                        <div class="fw-bold text-gray-600 fs-12px line-h-12">Donor:</div>
                        <div class="fw-bold text-gray-800">{{$donor->judul}}</div>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div class="me-3 w-40px h-40px rounded-2 bg-gray-400 text-gray-600 d-md-flex d-none align-items-center justify-content-center">
                        <i class="fa fa-exclamation-triangle fa-lg"></i>
                    </div>
                    <div>
                        <div class="fw-bold text-gray-600 fs-12px line-h-12">Level:</div>
                        <div class="fw-bold text-gray-800">URGENT</div>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div class="me-3 w-40px h-40px rounded-2 bg-gray-400 text-gray-600 d-md-flex d-none align-items-center justify-content-center">
                        <i class="fa fa-phone fa-lg"></i>
                    </div>
                    <div>
                        <div class="fw-bold text-gray-600 fs-12px line-h-12">Kontak:</div>
                        <div class="fw-bold text-gray-800">{{$donor->getUser->nama}} : {{$donor->no_hp}}</div>
                    </div>
                </div>

                <form action="{{route('donordarah.ajukan')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id_donor" value="{{$donor->id}}">
                    <button type="submit" class="btn btn-green" onclick="return confirm('Apakah anda yakin untuk mengajukan donor ?');">Ajukan Donor</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection