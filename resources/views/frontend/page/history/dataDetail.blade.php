@extends('frontend.layouts.masterDetail')



@section('content')
<div class="row">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title text-center">Data Anda Sebagai Pendonor</h4>
            <div class="col-lg-12   ">
                    <div class="row gx-2">
                        <div class="col-lg-6 mb-2">
                            <label for="">Donor</label>
                            <input type="text" class="form-control form-control-lg rounded-2" value="{{$data->getDataDonor->judul}}" disabled>
                        </div>
                        <div class="col-lg-6 mb-2">
                            <label for="">No HP Donor</label>
                            <input type="text" class="form-control form-control-lg rounded-2" value="{{$data->getDataDonor->no_hp}}" disabled>
                        </div>
                    </div>
                    <div class="mb-2">
                        <label for="">Catatan</label>
                        <input type="text" class="form-control form-control-lg rounded-2" placeholder="Subject *" value="{{$data->catatan}}" disabled>
                    </div>
                    <div class="mb-20px pb-4px">
                        <label for="">Alamat</label>
                        <textarea class="form-control" rows="5" name="message" placeholder="Messages *" disabled>{{$data->getDataDonor->lokasi}}</textarea>
                    </div>
            </div>
        </div>
    </div>
</div>

@endsection