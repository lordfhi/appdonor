@extends('frontend.layouts.masterDetail')



@section('content')
<div class="row">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title text-center">Data Anda Sebagai Pendonor</h4>
            <table class="tablesaw table-bordered table-hover table tablesaw-swipe" data-tablesaw-mode="swipe" id="tablesaw-7641" style="">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Donor</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $data)
                    <tr>
                        <td class="">1</td>
                        <td class="">{{$data->getDataDonor->judul}}</td>
                        <td class="">
                            @if ($data->status == 2)
                                <P class="text-success">Berhasil</P>
                                @elseif($data->status == 3)
                                <P class="danger">Gagal</P>
                                @else
                                <p>Menunggu Approval</p>
                                @endif
                        </td>
                        <td><a href="{{route('history.detail',$data->id_users)}}" class="btn btn-primary">Detail</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection