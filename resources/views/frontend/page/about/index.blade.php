@extends('frontend.layouts.masterDetail')

@section('content')
<div class="row">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title text-center">About Us</h4>
            <div class="col-lg-12   ">
                    @foreach ($about as $row)
                        <p align="justify">{{$row->deskripsi}}</p>
                    @endforeach
            </div>
        </div>
    </div>
</div>
@endsection