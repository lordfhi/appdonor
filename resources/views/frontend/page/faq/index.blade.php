@extends('frontend.layouts.masterDetail')


@section('content')
<div class="accordion faq" id="faq-list">
    <!-- BEGIN card -->
    <b><h1><center>FAQ</center></h1></b>
    @foreach ($faq as $row)
    <div class="accordion-item bg-gray-800 border-0">
        <div class="accordion-header">
            <a href="#" class="accordion-button bg-gray-400 text-gray-800 fw-bold shadow-none" id="faq-{{$row->id}}-heading" data-bs-toggle="collapse" data-bs-target="#faq-{{$row->id}}">
                <i class="fa fa-lg fa-question-circle fa-fw text-muted me-3"></i>{{$row->judul}}
            </a>
        </div>
        <div id="faq-{{$row->id}}" class="collapse" data-bs-parent="#faq-list">
            <div class="accordion-body bg-white">
                <p class="mb-0">
                  {{$row->deskripsi}}
                </p>
            </div>
        </div>
    </div>
    @endforeach
    <!-- END card -->
</div>
@endsection