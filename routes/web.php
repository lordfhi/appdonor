<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// BackEnd
use App\Http\Controllers\backend\SiteController;
use App\Http\Controllers\backend\FAQController;
use App\Http\Controllers\backend\AboutController;
use App\Http\Controllers\backend\KategoriController;
use App\Http\Controllers\backend\IklanController;
use App\Http\Controllers\backend\RDController;
// FrontEnd
use App\Http\Controllers\frontend\MainController;
use App\Http\Controllers\frontend\ApplyDonorController;
use App\Http\Controllers\frontend\HistoryController;
use App\Http\Controllers\LoginController;

Route::get('/',[MainController::class,'index'])->name('home');
Route::get('/donor-darah/{id}',[MainController::class,'donordarah'])->name('donordarah');
Route::get('/donor',[MainController::class,'donor'])->name('donor');
Route::get('/data-detail/{id}',[MainController::class,'detail'])->name('donordarah.detail');
Route::get('/abouts',[MainController::class,'about'])->name('abouts');
Route::get('/faq',[MainController::class,'faq'])->name('faq');

// AUTHENTICATE

Route::get('/login',[LoginController::class,'login'])->name('login');
Route::get('/logout',[LoginController::class,'logout'])->name('logout');
Route::post('/auth',[LoginController::class,'auth'])->name('auth');
Route::get('/register',[LoginController::class,'register'])->name('register');
Route::post('/save-register',[LoginController::class,'saveRegister'])->name('register.save');


Route::group(['middleware' => ['auth']], function(){
    
    // Dashboard
    Route::get('/dashboard',[SiteController::class,'dashboard'])->name('dashboard');
    
    // KATEGORI
    Route::get('/kategori',[KategoriController::class,'index'])->name('kategori');
    Route::get('/kategori-tambah',[KategoriController::class,'addForm'])->name('kategori.add');
    Route::post('/kategori-save',[KategoriController::class,'saveData'])->name('kategori.save');
    Route::get('/kategori-edit/{id}',[KategoriController::class,'editForm'])->name('kategori.edit');
    Route::post('/kategori-update',[KategoriController::class,'updateData'])->name('kategori.update');
    Route::get('/kategori-delete/{id}',[KategoriController::class,'delete'])->name('kategori.delete');
    
    // IKLAN
    Route::get('/iklan-list',[IklanController::class,'index'])->name('iklan');
    Route::get('/iklan-tambah',[IklanController::class,'addForm'])->name('iklan.add');
    Route::get('/iklan-edit/{id}',[IklanController::class,'editForm'])->name('iklan.edit');
    Route::post('/iklan-save',[IklanController::class,'saveData'])->name('iklan.save');
    Route::post('/iklan-update',[IklanController::class,'update'])->name('iklan.update');
    Route::get('/iklan-delete/{id}',[IklanController::class,'delete'])->name('iklan.delete');
    
    // REQUEST DONOR
    Route::get('/request-donor',[RDController::class,'index'])->name('requestDonor');
    Route::get('/request-donor/{id}',[RDController::class,'ApproveDonor'])->name('requestDonor.approve');
    Route::post('/request-donor/verifikasi',[RDController::class,'VerifikasiDonor'])->name('requestDonor.verifikasi');
    
    
    // APPLY DONOR
    Route::post('/ajukan-donor',[ApplyDonorController::class,'applyData'])->name('donordarah.ajukan');
    
    // HISTORY
    Route::get('/history-data/{id}',[HistoryController::class,'index'])->name('history');
    Route::get('/history-data-detail/{id}',[HistoryController::class,'detailData'])->name('history.detail');
    
    // About
    Route::get('/about',[AboutController::class,'index'])->name('about');
    Route::get('/about-edit/{id}',[AboutController::class,'edit'])->name('about.edit');
    Route::post('/about-update',[AboutController::class,'update'])->name('about.update');
    
    // FAQ
    Route::get('/faqs',[FAQController::class,'index'])->name('faqs');
    Route::get('/faqs-add',[FAQController::class,'create'])->name('faqs.create');
    Route::post('/faqs-save',[FAQController::class,'save'])->name('faqs.save');
    Route::get('/faqs-edit/{id}',[FAQController::class,'edit'])->name('faqs.edit');
    Route::post('/faqs-update',[FAQController::class,'update'])->name('faqs.update');
    Route::get('/faqs-delete/{id}',[FAQController::class,'delete'])->name('faqs.delete');

});